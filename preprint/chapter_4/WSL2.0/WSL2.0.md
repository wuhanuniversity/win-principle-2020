# WSL2.0 

## 一、WSL简介

Windows Subsystem for Linux（简称WSL）是一个在Windows 10上能够运行原生Linux二进制可执行文件（ELF格式）的兼容层。它是由微软与Canonical公司合作开发，其目标是使纯正的Ubuntu 14.04 "Trusty Tahr"映像能下载和解压到用户的本地计算机，并且映像内的工具和实用工具能在此子系统上原生运行。



## 二、WSL历史

在设计之初，微软就允许类似于Win32这种子系统运行于windows NT内核之上，它可以为上层应用提供编程接口，同时避免应用去实现内核里的一些调用细节。NT内核的设计在最开始就可以支持POSIX，OS/2和win32子系统。

早先的子系统是用户态模块的实现，它封装了NT系统的系统调用为应用程序提供编程接口。所有的应用程序都是PE/COFF（一些为子系统封装NT系统调用的库和服务）可执行的。当一个用户态的程序启动的时候，启动器就会基于可执行的头部去引用适当的子系统来满足应用程序的依赖。

后来版本的子系统替换掉了POSIX层，由用户态组件提供了Subsystem for Unix-based Applications (SUA)，满足：

\1. 进程和信号管理

\2. 终端管理

\3. 系统服务请求和进程间通信

SUA的主要目的是为了鼓励应用程序移植到Windows上能尽量少的重写。这已经通过实现POSIX用户态API达到了。考虑到这些组件是用户态实现，很难跟内核态的系统调用（比如fork()）在语义上和效率上完全相对应。因为这种模式需要程序重新编译，它需要持续的功能移植，维护也是负担。

随着时间的演变，这些早先的子系统都退出历史舞台了。但是因为Windows NT内核的架构允许新的子系统环境，我们就基于这领域的原始积累进行扩展，发展Windows Subsystem for Linux。



## 三、系统调用

WSL基于Windows NT内核虚拟了Linux内核接口，这允许它执行未经修改的Linux ELF64二进制文件。一类内核接口是系统调用。系统调用是内核为用户态程序提供的一种服务。Linux内核和Windows NT内核都为用户态程序提供了几百个系统调用，但是他们有不同的语义，并且一般来说并不直接兼容。比如Linux提供fork, open和kill，Windows NT提供相兼容NtCreateProcess, NtOpenFile和 NtTerminateProcess。

Windows Subsystem for Linux 包含内核态驱动（lxss.sys和 lxcore.sys），以协调Linux系统调用的请求与Windows NT内核。驱动不包含Linux内核代码，但是是一个全新实现的Linux兼容的内核接口。在原生的Linux上，用户态程序请求一个系统调用，系统调用请求由Linux内核处理。在WSL，当一个系统调用由同一个可执行文件请求时，Windows NT内核把请求发送给lxcore.sys。 当可能时，lxcore.sys将Linux系统调用翻译成等价的Windows NT的调用，由它来完成繁重的工作。当没有可能的等价转换时，Windows内核态驱动需要直接处理请求。

比如说，Linux中的fork()系统调用没有直接的等价的windows版本。当一个fork系统调用由Windows Subsystem for Linux产生时，lxcore.sys需要做一些复制进程的准备工作，然后调用Windows NT内核APIs来产生一个进程来正确实现fork操作，完成为新进程复制额外的数据。



## 四、WSL2.0的安装并运行Docker

在Windows 10 2004版本，微软更新WSL到了2.0，WSL 2.0已经拥有了完整的Linux内核。

### （一）开启WSL

以管理员运行Powershell（开启WSL，如已开启可跳过）

```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform --启用“虚拟机平台”可选组件
```

默认使用wsl2

```
wsl --set-default-version 2
```

### （二）安装Ubuntu

 打开Microsoft Store，搜索ubuntu并安装。

![1](C:\Users\lenovo\Desktop\WSL2.0\src\1.png)

然后，在Power Shell中输入wsl，来启动刚刚安装的Ubuntu。

### （三）安装docker

打开刚刚安装的Ubuntu，安装依赖：

```
sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common

-- 信任 Docker 的 GPG 公钥：
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

-- 对于 amd64 架构的计算机，添加软件仓库：
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu \
   $(lsb_release -cs) \
   stable"-- 安装docker
sudo apt-get update
sudo apt-get install docker-ce
```

### （四）启动docker

这里跟Linux中略有不同，Linux中用的是systemctl start docker：

```
sudo service docker start
```

启动成功后，我们可以用一些常用的docker命令来测试docker是否启动成功，如：

```
docker images
docker search nginx
```

### （五）启动docker

最简单的测试方法，运行docker的helloworld，命令如下：

```
docker run hello-world
```

这里使用了nginx的镜像进行了测试，命令如下：

```
docker pull nginx
docker run --name nginx -p 8080:80 -d nginx
```

运行成功后，可以查看WSL的ip地址，在浏览器中输入内网IP地址 + 端口号进行浏览！



## 五、WSL 2 的体验变化

首次开始使用 WSL 时，你会注意到一些用户体验变化。以下是此预览中两个最重要的变化。

将 Linux 文件放在 Linux 根文件系统中

确保将 Linux 应用程序中经常访问的文件放在 Linux 根文件系统中，以享受文件性能优势。过去微软一直强调在使用 WSL 1 时将文件放入 C 盘，但 WSL 2 中的情况并非如此。要享受 WSL 2 中更快的文件系统访问权限，这些文件必须放在 Linux 根文件系统里面。而且现在 Windows 应用程序可以访问 Linux 根文件系统（如文件资源管理器！尝试在 Linux 发行版的主目录中运行：explorer.exe . 看看会发生什么），这将使这种转换变得更加容易。

在初始构建中使用动态 IP 地址访问 Linux 网络应用程序

WSL 2 做了架构的巨大变更，使用了虚拟化技术，并仍在努力改进网络支持。由于 WSL 2 现在运行在虚拟机中，因此你从 Windows 访问 Linux 网络应用程序需要使用该 VM 的 IP 地址，反之亦然，你需要 Windows 主机的 IP 地址才能从 Linux 中访问 Windows 网络应用程序。 WSL 2 的目标是尽可能使用 localhost 访问网络应用程序！可以在[文档中](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Fwindows%2Fwsl%2Fwsl2-ux-changes%23accessing-network-applications)找到有关如何执行此操作的完整详细信息和步骤。



## 六、新的 WSL 命令

WSL 添加了一些新命令选项来帮助控制和查看 WSL 版本和发行版。

除了上面提到的 --set-version 和 --set-default-version 之外，还有：

wsl --shutdown

立即终止所有正在运行的发行版和 WSL 2 轻量级实用程序虚拟机。

一般来说，支持 WSL 2 发行版的虚拟机是由 WSL 来管理的，因此会在需要时将其打开并在不需要时将其关闭。但也可能存在你希望手动关闭它的情况，此命令允许你通过终止所有发行版并关闭 WSL 2 虚拟机来执行此操作。

wsl --list --quiet

仅列出发行版名称。此命令对于脚本编写很有用，因为它只会输出你已安装的发行版的名称，而不显示其他信息，如默认发行版、版本等。

wsl --list --verbose

显示有关所有发行版的详细信息。此命令列出每个发行版的名称，发行版所处的状态以及正在运行的版本。默认发行版标以星号。

