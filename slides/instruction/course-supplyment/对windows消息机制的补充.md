# 对windows消息机制的补充

## 消息机制

#### 消息有哪几种？

其实，windows中的消息虽然很多，但是种类并不繁杂，大体上有3种：窗口消息、命令消息和控件通知消息。

**窗口消息**

窗口消息大概是系统中最为常见的消息，它是指由操作系统和控制其他窗口的窗口所使用的消息。例如CreateWindow()、DestroyWindow()和MoveWindow()等都会激发窗口消息，还有我们在上面谈到的单击鼠标所产生的消息也是一种窗口消息。

**命令消息**

这是一种特殊的窗口消息，它用来处理从一个窗口发送到另一个窗口的用户请求，例如按下一个按钮，他就会向主窗口发送一个命令消息。

**控件通知消息**

控件通知消息是指这样一种消息，一个窗口内的子控件发生了一些事情，需要通知父窗口。通知消息只适用于标准的窗口控件如按钮、列表框、组合框、编辑框，以及Windows公共控件如树状视图、列表视图等。例如，单击或双击一个控件、在控件中选择部分文本、操作控件的滚动条都会产生通知消息。她类似于命令消息，当用户与控件窗口交互时，那么控件通知消息就会从控件窗口发送到它的主窗口。但是这种消息的存在并不是为了处理用户命令，而是为了让主窗口能够改变控件，例如加载、显示数据。例如按下一个按钮，他向父窗口发送的消息也可以看作是一个控件通知消息；单击鼠标所产生的消息可以由主窗口直接处理，然后交给控件窗口处理。

其中窗口消息及控件通知消息主要由窗口类，即直接或间接由CWND类的派生类，处理。相对窗口消息及控件通知消息而言，命令消息的处理对象范围就广得多，它不仅可以由窗口类处理，还可以由文档类，文档模板类及应用类所处理。

一般控件通知消息用到的比较多，我把常见的几个列出来供大家参考：

    按扭控件
    BN_CLICKED        用户单击了按钮
    BN_DISABLE 按钮被禁止
    BN_DOUBLECLICKED  用户双击了按钮
    BN_HILITE  用户加亮了按钮
    BN_PAINT  按钮应当重画
    BN_UNHILITE 加亮应当去掉

    组合框控件
    CBN_CLOSEUP 组合框的列表框被关闭
    CBN_DBLCLK 用户双击了一个字符串
    CBN_DROPDOWN 组合框的列表框被拉出
    CBN_EDITCHANGE 用户修改了编辑框中的文本
    CBN_EDITUPDATE 编辑框内的文本即将更新
    CBN_ERRSPACE 组合框内存不足
    CBN_KILLFOCUS 组合框失去输入焦点
    CBN_SELCHANGE 在组合框中选择了一项
    CBN_SELENDCANCEL 用户的选择应当被取消
    CBN_SELENDOK 用户的选择是合法的
    CBN_SETFOCUS 组合框获得输入焦点

    编辑框控件
    EN_CHANGE 编辑框中的文本己更新
    EN_ERRSPACE 编辑框内存不足
    EN_HSCROLL 用户点击了水平滚动条
    EN_KILLFOCUS 编辑框正在失去输入焦点
    EN_MAXTEXT 插入的内容被截断
    EN_SETFOCUS 编辑框获得输入焦点
    EN_UPDATE 编辑框中的文本将要更新
    EN_VSCROLL 用户点击了垂直滚动条消息含义

    列表框控件
    LBN_DBLCLK 用户双击了一项
    LBN_ERRSPACE 列表框内存不够
    LBN_KILLFOCUS 列表框正在失去输入焦点
    LBN_SELCANCEL 选择被取消
    LBN_SELCHANGE 选择了另一项
    LBN_SETFOCUS 列表框获得输入焦点

**队列消息和非队列消息**

从消息的发送途径来看，消息可以分成2种：队列消息和非队列消息。消息队列由可以分成系统消息队列和线程消息队列。系统消息队列由Windows维护，线程消息队列则由每个GUI线程自己进行维护，为避免给non-GUI现成创建消息队列，所有线程产生时并没有消息队列，仅当线程第一次调用GDI函数时系统才给线程创建一个消息队列。队列消息送到系统消息队列，然后到线程消息队列；非队列消息直接送给目的窗口过程。

对于队列消息，最常见的是鼠标和键盘触发的消息，例如*WM_MOUSERMOVE*,*WM_CHAR*等消息，还有一些其它的消息，例如：*WM_PAINT*、 *WM_TIMER*和*WM_QUIT*。当鼠标、键盘事件被触发后，相应的鼠标或键盘驱动程序就会把这些事件转换成相应的消息，然后输送到系统消息队列，由 Windows系统去进行处理。Windows系统则在适当的时机，从系统消息队列中取出一个消息，根据MSG消息结构确定消息是要被送往那个窗口，然后把取出的消息送往创建窗口的线程的相应队列，下面的事情就该由线程消息队列操心了，Windows开始忙自己的事情去了。线程看到自己的消息队列中有消息，就从队列中取出来，通过操作系统发送到合适的窗口过程去处理。
一般来讲，系统总是将消息Post在消息队列的末尾。这样保证窗口以先进先出的顺序接受消息。**然而,WM_PAINT是一个例外，同一个窗口的多个 WM_PAINT被合并成一个 WM_PAINT 消息, 合并所有的无效区域到一个无效区域。合并WM_PAIN的目的是为了减少刷新窗口的次数。**

非队列消息将会绕过系统队列和消息队列，直接将消息发送到窗口过程，。系统发送非队列消息通知窗口，系统发送消息通知窗口。例如,当用户激活一个窗口系统发送*WM_ACTIVATE*, *WM_SETFOCUS*, 和 *WM_SETCURSOR*。这些消息通知窗口它被激活了。非队列消息也可以由当应用程序调用系统函数产生。例如,当程序调用SetWindowPos时，系统发送*WM_WINDOWPOSCHANGED*消息。一些函数也发送非队列消息。

#### 消息的发送

发送消息的函数有SendMessage()、SendMessageCallback()、SendNotifyMessage()、SendMessageTimeout()；

寄送消息的函数主要有PostMessage()、PostThreadMessage()、 PostQuitMessage()；

广播消息的函数有BroadcastSystemMessage()、 BroadcastSystemMessageEx()。

SendMessage()的原型如下：`LRESULT SendMessage(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM lParam)`，这个函数主要是向一个或多个窗口发送一条消息，一直等到消息被处理之后才会返回。不过需要注意的是，如果接收消息的窗口是同一个应用程序的一部分，那么这个窗口的窗口函数就被作为一个子程序马上被调用；如果接收消息的窗口是被另外的线程所创建的，那么窗口系统就切换到相应的线程并且调用相应的窗口函数，这条消息不会被放进目标应用程序队列中。函数的返回值是由接收消息的窗口的窗口函数返回，返回的值取决于被发送的消息。

PostMessage()的原型如下：`BOOL PostMessage(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM lParam)`，该函数把一条消息放置到创建hWnd窗口的线程的消息队列中，该函数不等消息被处理就马上将控制返回。需要注意的是，如果hWnd参数为 HWND_BROADCAST，那么，消息将被寄送给系统中的所有的重叠窗口和弹出窗口，但是子窗口不会收到该消息；如果hWnd参数为NULL，则该函数类似于将dwThreadID参数设置成当前线程的标志来调用PostThreadMEssage函数。

消息的发送方式和寄送方式的区别所在：被发送的消息是否会被立即处理，函数是否立即返回。被发送的消息会被立即处理，处理完毕后函数才会返回；被寄送的消息不会被立即处理，他被放到一个先进先出的队列中，一直等到应用程序空线的时候才会被处理，不过函数放置消息后立即返回。

实际上，发送消息到一个窗口处理过程和直接调用窗口处理过程之间并没有太大的区别，他们直接的唯一区别就在于你可以要求操作系统截获所有被发送的消息，但是不能够截获对窗口处理过程的直接调用。

以寄送方式发送的消息通常是与用户输入事件相对应的，因为这些事件不是十分紧迫，可以进行缓慢的缓冲处理，例如鼠标、键盘消息会被寄送，而按钮等消息则会被发送。

广播消息用得比较少，BroadcastSystemMessage函数原型如下：`long BroadcastSystemMessage(DWORD dwFlags,LPDWORD lpdwRecipients,UINT uiMessage,WPARAM wParam,LPARAM lParam)`;该函数可以向指定的接收者发送一条消息，这些接收者可以是应用程序、可安装的驱动程序、网络驱动程序、系统级别的设备驱动消息和他们的任意组合。需要注意的是，如果dwFlags参数是BSF_QUERY并且至少一个接收者返回了BROADCAST_QUERY_DENY，则返回值为０，如果没有指定BSF_QUERY，则函数将消息发送给所有接收者，并且忽略其返回值。

#### 消息的接收

    BOOL GetMessage(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax);
    BOOL PeekMessage(LPMSG lpMsg, HWND hWnd, UINT wMsgFilterMin, UINT wMsgFilterMax, UINT wRemoveMsg);
    BOOL WaitMessage();

GetMessage()该函数用来获取与hWnd参数所指定的窗口相关的且wMsgFilterMin和wMsgFilterMax参数所给出的消息值范围内的消息。需要注意的是，如果hWnd为NULL，则GetMessage获取属于调用该函数应用程序的任一窗口的消息，如果 wMsgFilterMin和wMsgFilterMax都是０，则GetMessage就返回所有可得到的消息。函数获取之后将删除消息队列中的除 WM_PAINT消息之外的其他消息，至于WM_PAINT则只有在其处理之后才被删除。

PeekMessage()只是从消息队列中查询消息，如果有消息，则立刻返回true；没有则返回false。如果有消息，PeekMessage中wRemoveMsg参数中设置的是PM_REMOVE则在取出消息并将消息从队列中删除，若设置是PM_NOREMOVE消息就不会从消息队列中取出。

waitMessage()功能：当一个线程的消息队列中没有消息存在时,waitMessage函数会使该线程中断并处于等待状态，同时把控制权交给其它线程，直到被中断那个线程的消息队列中有了新的消息为止。


